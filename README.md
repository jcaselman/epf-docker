# Container Images for Eggplant Functional

This repository contains an example implementation of a GitLab CI pipeline demonstrating how the `fusion-engine-ubi8` container image from [quay.io](https://quay.io/) can be used to run an Eggplant Functional script in a CI pipeline.

You can fork this repository to experiment with it yourself, but you'll need to create an `EPF_LICENSE_KEY` GitLab CI variable containing a valid Eggplant Functional license key in the project settings on your fork.

The image is configured to run Eggplant Functional with the headless GNUstep backend, allowing it to run in a container without a GUI.  Bonjour discovery is disabled, as that makes little sense in a container environment (and is known to cause a crash!).

Prior to running a script, the EULA and the privacy policy are accepted, as well as installing a license, by running the relevant commands.

The container entrypoint can use environment variables to accept the EULA, privacy policy, and install a license. This example also maps a volume and runs a script (tested on Windows Powershell and macOS Terminal):
```
docker run -v ${PWD}:/tests -e EGGPLANT_ACCEPT_EULA=true -e EGGPLANT_ACCEPT_PRIVACY_AGREEMENT=true -e EGGPLANT_LICENSE_KEY=<license-key> quay.io/eggplantsoftware/fusion-engine-ubi8:23.4.103 runscript /tests/Demo.suite/Scripts/check_tutorial_sut.script
```

To start Eggdrive, run the following command, be sure you expose the driveport you are choosing:

```
runscript -driveport 5400 -CommandLineOutput yes
```

See [Runscript documentation](https://docs.eggplantsoftware.com/studio/epf-runscript-command-options) for more details.